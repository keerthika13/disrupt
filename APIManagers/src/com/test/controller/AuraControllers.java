package com.test.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.metlife.aura.integration.accessor.AuraAccessor;
import com.metlife.aura.integration.businessobjects.TransferObject;
import com.metlife.eapplication.common.JSONArray;
import com.metlife.eapplication.common.JSONObject;

public class AuraControllers {
	
	private List vectorQuestions = null;	
	private static AuraAccessor auraAccessor = null;
	private static String clientName = null;
	private static String questionnaireId = null;
	
	@RequestMapping(value = "/aura1",method = RequestMethod.POST,headers="Accept=application/json")	
	public ResponseEntity<List> intitiateQuestion(InputStream inputStream){		
    	BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
		String line = null;	
		String string = "";		
		List<TransferObject> transferObjectList = null;
		ResponseEntity<List> responseEntity = null;
		try {
			while ((line = in.readLine()) != null) {
				string += line + "\n";
				System.out.println("line>>"+line);
			}		
			//report= validateSchema("method3", string);
			AuraAccessor auraAccessor = new AuraAccessor();
			String inputXML = "<XML><AURATX><AuraControl><UniqueID>COLI0516WG-32566786</UniqueID><Company>metaus</Company><SetCode>93</SetCode><SubSet>3</SubSet><PostBackTo>https://awweb.au.metlifeasia.com/CAW2/jsp/public/toall/resultsProcess.jsp</PostBackTo><PostErrorsTo>https://awweb.au.metlifeasia.com/CAW2/jsp/public/toall/errorProcess.jsp</PostErrorsTo><SaveExitPage>https://awweb.au.metlifeasia.com/CAW2/jsp/public/toall/resultsProcess.jsp</SaveExitPage><ExtractLanguage/></AuraControl><QuestionFilters><QuestionFilter>Term-Direct</QuestionFilter><QuestionFilter>Term-Direct</QuestionFilter></QuestionFilters><PresentationOptions><BaseFormat>1</BaseFormat><BaseNumbering>1</BaseNumbering><BaseStartAtNumber>1</BaseStartAtNumber><DetailNumbering>3</DetailNumbering><DetailFormat>1</DetailFormat><AutoPositionTop>1</AutoPositionTop><OnlyShowActiveBranch>0</OnlyShowActiveBranch><InterviewMode>1</InterviewMode><ForceBranchCompletion>0</ForceBranchCompletion><ForceOptionalQuestions>0</ForceOptionalQuestions><Branding>default</Branding><HeightUnits>ft.in,m.cm</HeightUnits><WeightUnits>lb,st.lb,kg</WeightUnits><SpeedUnits>mph,kmph</SpeedUnits><DistanceUnits>ft,m</DistanceUnits><DateFormat>dd/mm/yyyy</DateFormat><SeasonSpring/><SeasonSummer/><SeasonFall/><SeasonWinter/></PresentationOptions><EngineVariables><Variable Name=\"LOB\">Direct</Variable></EngineVariables><Insureds><Insured InterviewSubmitted=\"false\" MaritalStatus=\"Single\" Name=\"asdasd\" Smoker=\"No\" UniqueId=\"1\" Waived=\"false\"><Products><Product AgeCoverageAmount=\"100000.0\" AgeDistrCoverageAmount=\"100000.0\" CoverageAmount=\"100000.0\" FinancialCoverageAmount=\"100000.0\">Term-Direct</Product><Product AgeCoverageAmount=\"100000.0\" AgeDistrCoverageAmount=\"100000.0\" CoverageAmount=\"100000.0\" FinancialCoverageAmount=\"100000.0\">Term-Direct</Product></Products><EngineVariables><Variable Name=\"Gender\">Male</Variable><Variable Name=\"Age\">33</Variable><Variable Name=\"Occupation\"/><Variable Name=\"SpecialTerms\">No</Variable><Variable Name=\"PermEmploy\"/><Variable Name=\"Smoker\">No</Variable><Variable Name=\"Hours15\"/><Variable Name=\"Country\"/><Variable Name=\"Salary\"/><Variable Name=\"SumInsuredTerm\">100000</Variable><Variable Name=\"SumInsuredTPD\">0.0</Variable><Variable Name=\"SumInsuredTrauma\">0.0</Variable><Variable Name=\"SumInsuredIP\">0.0</Variable><Variable Name=\"Partner\">Coles</Variable><Variable Name=\"HideFamilyHistory\"/><Variable Name=\"HidePastTime\"/><Variable Name=\"GroupPool\"/><Variable Name=\"SchemeName\">Coles Online</Variable><Variable Name=\"FormLength\">Short</Variable><Variable Name=\"OccupationScheme\">None</Variable><Variable Name=\"HaveOtherCover\">No</Variable></EngineVariables></Insured></Insureds></AURATX></XML>";
			transferObjectList= 	auraAccessor.getQuestionnaire(inputXML);
			responseEntity= new ResponseEntity<List>(transferObjectList,HttpStatus.OK);
					
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		return responseEntity;
	}
	
	@RequestMapping(value = "/aura2",method = RequestMethod.POST,headers="Accept=application/json")	
	public ResponseEntity<List> processResponse(InputStream inputStream){		
    	BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
		String line = null;	
		String string = "";		
		JSONObject jsonObject = null;		
		ResponseEntity<List> responseEntity = null;
		ExecutorService executor = null;
		final Map<Integer, String[]> responses = new HashMap<Integer, String[]>();
		FutureTask<String> task = null;		
		try {		
			executor = Executors.newSingleThreadExecutor();
			while ((line = in.readLine()) != null) {
				string += line + "\n";
				System.out.println("line>>"+line);
			}		
			jsonObject = new JSONObject(string);		
			JSONArray arr = (JSONArray) jsonObject.get("responses");	
			List<String> list = new ArrayList<String>();
			for(int i = 0; i < arr.length(); i++){
				System.out.println("arr.getJSONObject(i).toString()>>"+arr.getString(i));
				list.add(arr.getString(i));
			 
			}	
			clientName = (String)jsonObject.get("clientName");
			questionnaireId = (String)jsonObject.get("questionnaireId");
			System.out.println("claimNumber>>"+jsonObject.get("clientName"));
			System.out.println("memberNumber>>"+jsonObject.get("questionnaireId"));
			
			responses.put((Integer)jsonObject.get("questionId"), (String[]) list.toArray(new String[list.size()]));
			auraAccessor = new AuraAccessor();
			/*String inputXML = (String)jsonObject.get("inputXml");//"<XML><AURATX><AuraControl><UniqueID>COLI0516WG-32566786</UniqueID><Company>metaus</Company><SetCode>93</SetCode><SubSet>3</SubSet><PostBackTo>https://awweb.au.metlifeasia.com/CAW2/jsp/public/toall/resultsProcess.jsp</PostBackTo><PostErrorsTo>https://awweb.au.metlifeasia.com/CAW2/jsp/public/toall/errorProcess.jsp</PostErrorsTo><SaveExitPage>https://awweb.au.metlifeasia.com/CAW2/jsp/public/toall/resultsProcess.jsp</SaveExitPage><ExtractLanguage/></AuraControl><QuestionFilters><QuestionFilter>Term-Direct</QuestionFilter><QuestionFilter>Term-Direct</QuestionFilter></QuestionFilters><PresentationOptions><BaseFormat>1</BaseFormat><BaseNumbering>1</BaseNumbering><BaseStartAtNumber>1</BaseStartAtNumber><DetailNumbering>3</DetailNumbering><DetailFormat>1</DetailFormat><AutoPositionTop>1</AutoPositionTop><OnlyShowActiveBranch>0</OnlyShowActiveBranch><InterviewMode>1</InterviewMode><ForceBranchCompletion>0</ForceBranchCompletion><ForceOptionalQuestions>0</ForceOptionalQuestions><Branding>default</Branding><HeightUnits>ft.in,m.cm</HeightUnits><WeightUnits>lb,st.lb,kg</WeightUnits><SpeedUnits>mph,kmph</SpeedUnits><DistanceUnits>ft,m</DistanceUnits><DateFormat>dd/mm/yyyy</DateFormat><SeasonSpring/><SeasonSummer/><SeasonFall/><SeasonWinter/></PresentationOptions><EngineVariables><Variable Name=\"LOB\">Direct</Variable></EngineVariables><Insureds><Insured InterviewSubmitted=\"false\" MaritalStatus=\"Single\" Name=\"asdasd\" Smoker=\"No\" UniqueId=\"1\" Waived=\"false\"><Products><Product AgeCoverageAmount=\"100000.0\" AgeDistrCoverageAmount=\"100000.0\" CoverageAmount=\"100000.0\" FinancialCoverageAmount=\"100000.0\">Term-Direct</Product><Product AgeCoverageAmount=\"100000.0\" AgeDistrCoverageAmount=\"100000.0\" CoverageAmount=\"100000.0\" FinancialCoverageAmount=\"100000.0\">Term-Direct</Product></Products><EngineVariables><Variable Name=\"Gender\">Male</Variable><Variable Name=\"Age\">33</Variable><Variable Name=\"Occupation\"/><Variable Name=\"SpecialTerms\">No</Variable><Variable Name=\"PermEmploy\"/><Variable Name=\"Smoker\">No</Variable><Variable Name=\"Hours15\"/><Variable Name=\"Country\"/><Variable Name=\"Salary\"/><Variable Name=\"SumInsuredTerm\">100000</Variable><Variable Name=\"SumInsuredTPD\">0.0</Variable><Variable Name=\"SumInsuredTrauma\">0.0</Variable><Variable Name=\"SumInsuredIP\">0.0</Variable><Variable Name=\"Partner\">Coles</Variable><Variable Name=\"HideFamilyHistory\"/><Variable Name=\"HidePastTime\"/><Variable Name=\"GroupPool\"/><Variable Name=\"SchemeName\">Coles Online</Variable><Variable Name=\"FormLength\">Short</Variable><Variable Name=\"OccupationScheme\">None</Variable><Variable Name=\"HaveOtherCover\">No</Variable></EngineVariables></Insured></Insureds></AURATX></XML>";
			System.out.println("inputXML>>"+inputXML);
			vectorQuestions = auraAccessor.getQuestionnaire(inputXML);*/
			if (null != responses) {
				task = new FutureTask<String>(new Callable<String>() {
					public String call() throws Exception {
						try {
							vectorQuestions = auraAccessor.processResponse(clientName, questionnaireId,
									responses);									
							return "Success";
						} finally {
							
						}
					}
				});				
				executor.execute(task);
				if(null!=task){
					task.get(60*3, TimeUnit.SECONDS);
				}								
			}
			
			responseEntity= new ResponseEntity<List>(vectorQuestions,HttpStatus.OK);
					
		}  catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();			
		}finally {
			if (executor != null) {
				executor.shutdown();
			}
		}		
		return responseEntity;
	}
}
