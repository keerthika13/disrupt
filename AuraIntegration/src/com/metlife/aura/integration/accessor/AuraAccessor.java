
package com.metlife.aura.integration.accessor;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.rpc.ServiceException;

import org.apache.axis.utils.Options;
import org.xml.sax.SAXException;

import com.metlife.aura.integration.businessobjects.ResponseObject;
import com.metlife.aura.integration.mappers.AuraRequestMapper;
import com.metlife.aura.integration.mappers.AuraResponseMapper;
import com.rgatp.aura.wsapi.AuraService;
import com.rgatp.aura.wsapi.AuraServiceProxyLocator;
import com.rgatp.aura.wsapi.Questionnaire;
import com.rgatp.aura.wsapi.Response;

public class AuraAccessor {
	private static final String PACKAGE_NAME = AuraAccessor.class.getPackage().getName();

	private static final String CLASS_NAME = AuraAccessor.class.getSimpleName();
	
	private AuraService auraService = null;
	private Questionnaire auraQuestionnaire = null;
	
	
	public AuraAccessor() {
		
	}
	

	/**
	 * This method is resposible for Initiatalizing Questionnaire from AURA. A
	 * call is made to AURA webservice for initiating AURA Questionnaire and
	 * Question set is returned on the basis of input.xml values.
	 * 
	 * @param inputXML
	 * @return
	 * @throws RemoteException
	 * @throws ServiceException 
	 * @throws MalformedURLException 
	 */
	public List getQuestionnaire(String inputXML) throws RemoteException, MalformedURLException, ServiceException {
		final String METHOD_NAME = "getQuestionnaire";
		
		 getAuraService();
		
		auraQuestionnaire = auraService.initiateQuestionnaire(inputXML);
		
		List transferObjectList = null; 
		
		transferObjectList = AuraResponseMapper.populateDTO(auraQuestionnaire.getQuestionnaireId(), auraQuestionnaire.getClientName(), auraQuestionnaire.getQuestions());
		
		
		return transferObjectList;
	}

	/**
	 * This Method makes a Aura webservice call to process the question responses and Get the  updated question set with
	 * reflexive questions if applicable.
	 * @param clientName
	 * @param questionnaireId
	 * @param hashTableResponses
	 * @return
	 * @throws RemoteException
	 * @throws ServiceException 
	 * @throws MalformedURLException 
	 */
	public List processResponse(String clientName, String questionnaireId, Map<Integer, String[]> hashTableResponses) throws RemoteException, MalformedURLException, ServiceException {
		final String METHOD_NAME = "processResponse";
		Response[] responses = AuraRequestMapper.prepareResponses(hashTableResponses);
		if(null==auraService){
			auraService = getAuraService();
		}
		auraQuestionnaire = auraService.processResponses(clientName, questionnaireId, responses);
		return AuraResponseMapper.populateDTO(questionnaireId, clientName,auraQuestionnaire.getQuestions());//changed
	}
	
	/**
	 * This method makes a Aura webservice call to submit responses to Aura questionnaire.
	 * @param clientname
	 * @param questionnaireId
	 * @return
	 * @throws com.chordiant.service.ServiceException 
	 * @throws MetlifeException 
	 * @throws ServiceException 
	 * @throws ParserConfigurationException 
	 * @throws IOException 
	 * @throws SAXException 
	 */
	/*public ResponseObject submitQuestionnaire(String clientname, String questionnaireId, HashMap ruleMap, String lob, String partner, String username, String authToken, ApplicationDTO applicationDTO) throws com.chordiant.service.ServiceException, MetlifeException, ServiceException, SAXException, IOException, ParserConfigurationException {
		final String METHOD_NAME = "submitQuestionnaire";
		LogHelper.methodEntry(PACKAGE_NAME, CLASS_NAME, METHOD_NAME);
		if(null==auraService){
			auraService = getAuraService();
		}
		StringBuffer productdecisionBuffer = new StringBuffer();
		String overallDecision = null;
		String output = auraService.submitQuestionnaire(clientname, questionnaireId);
		ResponseObject responseObject = AuraResponseMapper.populateResponse(output);
		responseObject.setQuestionnaireId(questionnaireId);
		EapplyClientAgent eapplyClientAgent = null;
		ApplicantDTO applicantDTO = null;
		InputData inputData = null;
		OutputData outputData = null;
		PostProcessRuleBO  ruleBO = null;
		Boolean clientMatch = false;		
		List<String> ratingReason = null;
		List<String> plannedReason = null;
		String productOrgDecision = null;
		Rating rating = null;
		String lobclientMatch = null;
		List<ItemData> enternalVariables = null;
		double extIPcoverAmt = 0.0;
		CoversDTO coverDto = null;
		
		DistributedMap cacheObject = null;
		InitialContext ctx;
		try {
		SRKR:19/11/2014: Using websphere cache to get the ruledata if not available in ruleMap. Mainly used during non member validation webservice calls.	
		ctx = new InitialContext();
		      cacheObject = (DistributedMap) ctx.lookup("WPS/RuleMap");
		} catch (Exception e) {
		      e.printStackTrace();
		}
		
			if(null != responseObject.getClientData() && null != responseObject.getClientData().getQuestionFilter() && responseObject.getClientData().getQuestionFilter().contains("Group")){
				lob = "Group";
				lobclientMatch = "Institutional";
			}else{
				lob = "Direct";
				lobclientMatch = "Individual";
			}
			
			if(responseObject.getClientData().getAppSubSetCode().equalsIgnoreCase("3")
					|| responseObject.getClientData().getAppSubSetCode().equalsIgnoreCase("4") 
					|| responseObject.getClientData().getAppSubSetCode().equalsIgnoreCase("5")
					||"CITI".equalsIgnoreCase(partner) 
					||"ACRF".equalsIgnoreCase(partner) 
					|| "Group".equalsIgnoreCase(lob)){			
					
				String[] reasons;
				
			for (int itr = 0; itr < responseObject.getClientData().getListInsured().size(); itr++) {
				Insured insured = (Insured) responseObject.getClientData().getListInsured().get(itr);				
				
				if (null != insured && null != insured.getListOverallDec()) {
					for (int insItr = 0; insItr < insured.getListOverallDec().size(); insItr++) {
						Decision decision = (Decision) insured.getListOverallDec().get(insItr);
						if (null != decision && null != decision.getProductName() && !"".equalsIgnoreCase(decision.getProductName().trim())) {
							
							ratingReason = new ArrayList<String>();
							plannedReason = new ArrayList<String>();
							ruleBO = new PostProcessRuleBO();
							productOrgDecision= decision.getDecision();
							EapplicationIndividualRuleHelper.RULE_FILE_LOC = ConfigurationHelper.getConfigurationValue("RULE_FILE_LOC", "RULE_FILE_LOC");
							if(null!=ruleMap && null!=ruleMap.get("PostProcessingMaster.xls")){
								ruleBO.setKbase((KnowledgeBase)ruleMap.get("PostProcessingMaster.xls"));
							}else if(cacheObject!=null && cacheObject.get("PostProcessingMaster.xls")!=null){
								SRKR:19/11/2014: Using websphere cache to get the ruledata if not available in ruleMap. Mainly used during non member validation webservice calls.
								ruleBO.setKbase((KnowledgeBase)cacheObject.get("PostProcessingMaster.xls"));
							}
							ruleBO.setRuleFileName("PostProcessingMaster.xls");
							//Basic Data set Here like LOB, Partner, Product Name
							ruleBO.setBrand(partner);
							ruleBO.setLob(lob);
							ruleBO.setProduct(decision.getProductName());
							//Step1 fire the rule mater here .
							ruleBO.setRuleFileName("PostProcessingMaster.xls");
							ruleBO = EapplicationIndividualRuleHelper.invokepostProcessRule(ruleBO);
							clientMatch = ruleBO.getClientMatch();
							System.out.println("After the rule sheet<>>>"+clientMatch);
							
							if(null!=applicationDTO && null!=applicationDTO.getApplicant()){
								for (int applicantItr = 0; applicantItr < applicationDTO.getApplicant().size(); applicantItr++) {
									applicantDTO = (ApplicantDTO)applicationDTO.getApplicant().get(applicantItr);
									if(null!=applicantDTO && "Primary".equalsIgnoreCase(applicantDTO.getApplicantType()) 
											&& null!=applicantDTO.getTransferCvr() && applicantDTO.getTransferCvr()
											&& clientMatch){
										System.out.println("loop 111");
										clientMatch= Boolean.FALSE;
									}
									System.out.println("partner>>"+partner);
									*//**
									 * If no existing cover then the application will be RUW in case of PWCS
									 *//*
									if(null != partner && "PWCS".equalsIgnoreCase(partner)){
										if (null != applicantDTO.getCovers()) {
											for(int itrcvr=0;itrcvr<applicantDTO.getCovers().size();itrcvr++){
												coverDto = (CoversDTO)applicantDTO.getCovers().get(itrcvr);											
												if("IP".equalsIgnoreCase(coverDto.getCoverNo()) && null != coverDto.getExistingCoverAmount() && !"".equalsIgnoreCase(coverDto.getExistingCoverAmount().trim())){
													extIPcoverAmt = new BigDecimal(coverDto.getExistingCoverAmount()).doubleValue();
												}
										}
									}
									}
								}
							}
							if(null!=applicationDTO.getRequestType() && ("UWCOVER".equalsIgnoreCase(applicationDTO.getRequestType())|| "ICOVER".equalsIgnoreCase(applicationDTO.getRequestType()))){
								System.out.println("Loop 2");
								clientMatch= Boolean.FALSE;
							}
							//Setting the data for the 2nd Rule
							ruleBO.setAuraPostProcessID(ruleBO.getAuraPostProcessID());
							ruleBO.setOriginalDecision(decision.getDecision());
							ruleBO.setTotalExlusion(decision.getListExclusion().size());
							ruleBO.setTotalLExlusion(decision.getLExclusions());
							ruleBO.setTotalMExlusion(decision.getMExclusions());
							
							//Setting the loadings
							int loadingValue = 0;
							if(null != decision.getTotalDebitsValue()){
								loadingValue = new Double(decision.getTotalDebitsValue()).intValue();
								decision.setOriginalTotalDebitsValue(""+loadingValue);
							}
							ruleBO.setTotalLoading(loadingValue);
							//Setp2 fire rule 2 here
							EapplicationIndividualRuleHelper.RULE_FILE_LOC = ConfigurationHelper.getConfigurationValue("RULE_FILE_LOC", "RULE_FILE_LOC");
							if(null!=ruleMap && null!=ruleMap.get("PostProcessing1.xls")){
								ruleBO.setKbase((KnowledgeBase)ruleMap.get("PostProcessing1.xls"));
							}else if(cacheObject!=null && cacheObject.get("PostProcessing1.xls")!=null){
								SRKR:19/11/2014: Using websphere cache to get the ruledata if not available in ruleMap. Mainly used during non member validation webservice calls.
								ruleBO.setKbase((KnowledgeBase)cacheObject.get("PostProcessing1.xls"));
							}							
							ruleBO.setRuleFileName("PostProcessing1.xls");
							ruleBO = EapplicationIndividualRuleHelper.invokepostProcessRule(ruleBO);
							
							if(null != ruleBO.getPostAURADecision()){
								decision.setDecision(ruleBO.getPostAURADecision());	
								reasons = new String[1];
								reasons[0] = ruleBO.getReason();
								decision.setReasons(reasons);	
								plannedReason.add(reasons[0]);
							}else{
								System.out.println("ruleBO.getPostAURADecision() 111 else"+ruleBO.getPostAURADecision());
							}
							
							
							
							ruleBO.setPostAURADecision(null);
							EapplicationIndividualRuleHelper.RULE_FILE_LOC = ConfigurationHelper.getConfigurationValue("RULE_FILE_LOC", "RULE_FILE_LOC");
							if(null!=ruleMap && null!=ruleMap.get("PostProcessing2.xls")){
								ruleBO.setKbase((KnowledgeBase)ruleMap.get("PostProcessing2.xls"));
							}else if(cacheObject!=null && cacheObject.get("PostProcessing2.xls")!=null){
								SRKR:19/11/2014: Using websphere cache to get the ruledata if not available in ruleMap. Mainly used during non member validation webservice calls.
								ruleBO.setKbase((KnowledgeBase)cacheObject.get("PostProcessing2.xls"));
							}							
							ruleBO.setRuleFileName("PostProcessing2.xls");
							ruleBO.setOriginalDecision(decision.getDecision());
							
							System.out.println("decision.getDecision() before 22"+decision.getDecision());
							
							//Fire rule 2s Here
							ruleBO = EapplicationIndividualRuleHelper.invokepostProcessRule(ruleBO);
							if(null != ruleBO.getPostAURADecision()){
								decision.setDecision(ruleBO.getPostAURADecision());	
								decision.setTotalDebitsReason(ruleBO.getReason());
								decision.setTotalDebitsValue(ruleBO.getPostAURALoading().toString());
								reasons = new String[1];
								reasons[0] = ruleBO.getReason();
								decision.setReasons(reasons);
								if(Double.parseDouble(decision.getTotalDebitsValue())>0.0){
									plannedReason.add(ruleBO.getReason());
								}
								
							}else{
								System.out.println("ruleBO.getPostAURADecision() 222 else>>"+ruleBO.getPostAURADecision());
							}
							productdecisionBuffer.append(decision.getDecision());
														
							*//**
							 * BMI post processing only for institutional now - 05012012 
							 *//*
							if(("group".equalsIgnoreCase(lob) && null != decision.getDecision()) || responseObject.getClientData().getAppSubSetCode().equalsIgnoreCase("5")){
								ruleBO.setPostAURADecision(null);
								EapplicationIndividualRuleHelper.RULE_FILE_LOC = ConfigurationHelper.getConfigurationValue("RULE_FILE_LOC", "RULE_FILE_LOC");
								if(null!=ruleMap && null!=ruleMap.get("PostProcessing3.xls")){
									ruleBO.setKbase((KnowledgeBase)ruleMap.get("PostProcessing3.xls"));
								}else if(cacheObject!=null && cacheObject.get("PostProcessing3.xls")!=null){
									SRKR:19/11/2014: Using websphere cache to get the ruledata if not available in ruleMap. Mainly used during non member validation webservice calls.
									ruleBO.setKbase((KnowledgeBase)cacheObject.get("PostProcessing3.xls"));
								}								
								ruleBO.setRuleFileName("PostProcessing3.xls");
								ruleBO.setOriginalDecision(decision.getDecision());
								ruleBO.setBrand(partner);
								*//** 
								 * Loop through the genderAgeBMI tag to get the BMI loadng  
								 *//*
							List<Decision> bmiList = null;
							if(responseObject.getClientData().getAppSubSetCode().equalsIgnoreCase("5")){
								bmiList = insured.getListGenderAgeBMIRangeTable();
							}else if ("group".equalsIgnoreCase(lob)){
								bmiList = insured.getListGenderAgeBMIDec();
							}
							
							int bmiLoadingValue = 0;
							int bmlMedicalLoading  = 0;
								for (int bmiins = 0; bmiins < bmiList.size(); bmiins++) {
									Decision bmidecision = bmiList.get(bmiins);
									
									if (null != bmidecision && null != bmidecision.getProductName() && null != bmidecision.getTotalDebitsValue() && !"".equalsIgnoreCase(bmidecision.getProductName().trim()) && decision.getProductName().equalsIgnoreCase(bmidecision.getProductName())) {
										if(null != bmidecision.getTotalDebitsValue()){
											bmiLoadingValue = new Double(bmidecision.getTotalDebitsValue()).intValue();
										}else{
											bmiLoadingValue = 0;
										}
										ruleBO.setBmiLoading(bmiLoadingValue);
										
										if(new BigDecimal(decision.getTotalDebitsValue()).intValue() >= bmiLoadingValue){
											bmlMedicalLoading = new BigDecimal(decision.getTotalDebitsValue()).subtract(new BigDecimal(bmidecision.getTotalDebitsValue())).intValue();	
										}
										
										//For BMI it should be Medical Loadings only.
										ruleBO.setTotalExlusion(decision.getMExclusions());
										
										
									}
								} // Endo of BMI LOOP
								int origLoadingValue = 0;
								for (int origins = 0; origins < insured.getListOriginalRuleDec().size(); origins++) {
									Decision origDecision = (Decision) insured.getListOriginalRuleDec().get(origins);
									
									if (null != origDecision && null != origDecision.getProductName() && !"".equalsIgnoreCase(origDecision.getProductName().trim()) && decision.getProductName().equalsIgnoreCase(origDecision.getProductName())) {
										
										if(null != origDecision.getTotalDebitsValue()){
											origLoadingValue = new Double(origDecision.getTotalDebitsValue()).intValue();
										}else{
											origLoadingValue = 0;
										}
									}
								}
								if(bmlMedicalLoading >= 0){
									ruleBO.setTotalLoading(bmlMedicalLoading);
									
								}else if(origLoadingValue > 0){
									ruleBO.setTotalLoading(origLoadingValue);
								}
								ruleBO = EapplicationIndividualRuleHelper.invokepostProcessRule(ruleBO);
								
								//capturing original decision from Aura
								if("ACC".equalsIgnoreCase(productOrgDecision) ){
									
									if(null!=decision.getListRating() && decision.getListRating().size()>0 ){
										for (int iter=0; iter<decision.getListRating().size(); iter++) {
											 rating = (Rating) decision.getListRating().get(iter);
											if(null!=rating && null!=rating.getReason() && rating.getReason().length()>0 && null!=decision.getTotalDebitsValue()
													&& decision.getTotalDebitsValue().length()>0 && Double.parseDouble(decision.getTotalDebitsValue()) > 0.0){
												ratingReason.add(rating.getReason());
											}									
										}
									}
									
									if(null!=decision.getListExclusion() && decision.getListExclusion().size()>0){
										for (Iterator iter = decision.getListExclusion()
												.iterator(); iter.hasNext();) {
											Exclusion exclusion = (Exclusion) iter.next();
											if(null!=exclusion && null!=exclusion.getReason()&& exclusion.getReason().length>0){
												for (int exItr = 0; exItr < exclusion.getReason().length; exItr++) {
													if(null!=exclusion.getReason()[exItr] && exclusion.getReason()[exItr].length()>0 && !"null".equalsIgnoreCase(exclusion.getReason()[exItr].toString())){
														ratingReason.add(exclusion.getReason()[exItr]);
													}
													
												}
											}
											
										}
										
									}
									decision.setOrgReason(ratingReason);
								}else if("DCL".equalsIgnoreCase(productOrgDecision) && null!=decision.getReasons() && decision.getReasons().length>0){
									for (int exItr = 0; exItr < decision.getReasons().length; exItr++) {
										ratingReason.add(decision.getReasons()[exItr]);
									}
									decision.setOrgReason(ratingReason);
								}else if("RUW".equalsIgnoreCase(productOrgDecision)){
									
									for (int iter=0; iter<decision.getListRating().size(); iter++) {
										 rating = (Rating) decision.getListRating().get(iter);
										if(null!=rating && null!=rating.getReason() && rating.getReason().length()>0 && null!=decision.getTotalDebitsValue()
												&& decision.getTotalDebitsValue().length()>0 && Double.parseDouble(decision.getTotalDebitsValue()) > 0.0){
											ratingReason.add(rating.getReason());
											
										}									
									}
									decision.setOrgReason(ratingReason);	
								}
								
								if(null != ruleBO.getPostAURADecision()){
									decision.setDecision(ruleBO.getPostAURADecision());	
									reasons = new String[1];
									reasons[0] = ruleBO.getReason();
									decision.setReasons(reasons);
									plannedReason.add(ruleBO.getReason());
								}
								
								if(null!=plannedReason && plannedReason.size()>0){
									decision.setPlannedReason(plannedReason);
								}
								
								 // END of GROUP BMI Loop	
							}else{
								System.out.println("No third post process");
							}
							
							*//**
							 * Level 4 postProcessing for 15hrs questions.
							 *//*
							if("group".equalsIgnoreCase(lob) && null != partner && "SFPS".equalsIgnoreCase(partner) && null != decision.getProductName() && decision.getProductName().contains("IP") && "ACC".equalsIgnoreCase(decision.getDecision())){
								enternalVariables = insured.getListExtEngineVar();
								ItemData im = null;
								for(int l=0;l<enternalVariables.size();l++){									
									 im = enternalVariables.get(l);
									if(null != im.getCode() && "Hours15".equalsIgnoreCase(im.getCode()) && null != im.getValue() && "No".equalsIgnoreCase(im.getValue())){
											decision.setDecision("RUW");	
											reasons = new String[1];
											reasons[0] = "RUW, 15 hour eligibility";
											decision.setReasons(reasons);
											plannedReason.add(ruleBO.getReason());
										
										if(null!=plannedReason && plannedReason.size()>0){
											decision.setPlannedReason(plannedReason);
										}
									}
									
								}
							}
							
							*//**
							 * Level 5 postProcessing - only for PWCP Staff, When there is a existing cover will RUW the case
							 *//*
							System.out.println("extIPcoverAmt>>"+extIPcoverAmt);
							
							if("group".equalsIgnoreCase(lob) && null != partner && "PWCS".equalsIgnoreCase(partner) && extIPcoverAmt <= 0 && "ACC".equalsIgnoreCase(decision.getDecision())){
								decision.setDecision("RUW");	
								reasons = new String[1];
								reasons[0] = "RUW, existing cover nill";
								decision.setReasons(reasons);
								plannedReason.add(ruleBO.getReason());
							
								if(null!=plannedReason && plannedReason.size()>0){
									decision.setPlannedReason(plannedReason);
								
								}
							}
							*//**
							 * Level 6 postProcessing - When there is a change to conevrt to fixed from unitised
							 * 
							 * Check for the current formLength = short  and overall decision is DCL, then don't trigger this rule. 
							 * 
							 * 
							 *//*
							if("group".equalsIgnoreCase(lob)){						
								
								if(null!=applicationDTO && null!=applicationDTO.getApplicant()){
									for (int applicantItr = 0; applicantItr < applicationDTO.getApplicant().size(); applicantItr++) {
										if (null != applicantDTO.getCovers()) {
											for(int itrcvr=0;itrcvr<applicantDTO.getCovers().size();itrcvr++){
												coverDto = (CoversDTO)applicantDTO.getCovers().get(itrcvr);	
												if(null!=coverDto && applicantDTO.getAuradetails()!=null &&  "long".equalsIgnoreCase(applicantDTO.getAuradetails().getFormLength()) && coverDto.getCoverCategory()!=null && !coverDto.getCoverCategory().contains("Cancel")){
													// setting existing unit indicator		
													if(null != coverDto.getCoverExitUnit() && !"".equalsIgnoreCase(coverDto.getCoverExitUnit().trim()) && new BigDecimal(""+coverDto.getCoverExitUnit()).doubleValue()>0){
														ruleBO.setExistingUnit(Boolean.TRUE);
													}else{
														ruleBO.setExistingUnit(Boolean.FALSE);
													}												
													//setting additional unit indicator													
													ruleBO.setAdditionalUnit(coverDto.getAdditionalUnitInd());
													ruleBO.setPostAURADecision(null);
													EapplicationIndividualRuleHelper.RULE_FILE_LOC = ConfigurationHelper.getConfigurationValue("RULE_FILE_LOC", "RULE_FILE_LOC");
													if(null!=ruleMap && null!=ruleMap.get("PostProcessing4.xls")){
														ruleBO.setKbase((KnowledgeBase)ruleMap.get("PostProcessing4.xls"));
													}else if(cacheObject!=null && cacheObject.get("PostProcessing4.xls")!=null){
														SRKR:19/11/2014: Using websphere cache to get the ruledata if not available in ruleMap. Mainly used during non member validation webservice calls.
														ruleBO.setKbase((KnowledgeBase)cacheObject.get("PostProcessing4.xls"));
													}								
													ruleBO.setRuleFileName("PostProcessing4.xls");
													ruleBO.setLob(lob);
													ruleBO.setBrand(partner);
													ruleBO = EapplicationIndividualRuleHelper.invokepostProcessRule(ruleBO);
													if(null != ruleBO.getPostAURADecision() && "RUW".equalsIgnoreCase(ruleBO.getPostAURADecision())){
														decision.setDecision(ruleBO.getPostAURADecision());							
														reasons = new String[1];
														reasons[0] = ruleBO.getReason();
														decision.setReasons(reasons);							
														plannedReason.add(ruleBO.getReason());													
														if(null!=plannedReason && plannedReason.size()>0){
															decision.setPlannedReason(plannedReason);														
														}
														break;
													}
												}
											}
										}
									}
								}
								
							}					
							*//**
							 * Level 7 postProcessing - For transfers and Life events
							 *//*
							if("group".equalsIgnoreCase(lob)){	
								if(null!=applicationDTO && null!=applicationDTO.getRequestType()){
									
									EapplicationIndividualRuleHelper.RULE_FILE_LOC = ConfigurationHelper.getConfigurationValue("RULE_FILE_LOC", "RULE_FILE_LOC");
									if(null!=ruleMap && null!=ruleMap.get("PostProcessing4.xls")){
										ruleBO.setKbase((KnowledgeBase)ruleMap.get("PostProcessing4.xls"));
									}else if(cacheObject!=null && cacheObject.get("PostProcessing4.xls")!=null){
										SRKR:19/11/2014: Using websphere cache to get the ruledata if not available in ruleMap. Mainly used during non member validation webservice calls.
										ruleBO.setKbase((KnowledgeBase)cacheObject.get("PostProcessing4.xls"));
									}								
									ruleBO.setRuleFileName("PostProcessing4.xls");
									ruleBO.setExistingUnit(false);
									ruleBO.setAdditionalUnit(false);
									ruleBO.setLob(lob);
									ruleBO.setBrand(partner);
									ruleBO.setRequestType(applicationDTO.getRequestType());
									System.out.println("applicationDTO.getRequestType()>>"+applicationDTO.getRequestType());
									ruleBO = EapplicationIndividualRuleHelper.invokepostProcessRule(ruleBO);
									if(null != ruleBO.getPostAURADecision() && "RUW".equalsIgnoreCase(ruleBO.getPostAURADecision())){
										decision.setDecision(ruleBO.getPostAURADecision());							
										System.out.println("ruleBO.getPostAURADecision()>>"+ruleBO.getPostAURADecision());
										reasons = new String[1];
										reasons[0] = ruleBO.getReason();
										decision.setReasons(reasons);							
										plannedReason.add(ruleBO.getReason());													
										if(null!=plannedReason && plannedReason.size()>0){
											decision.setPlannedReason(plannedReason);														
										}
									}
								}
							}
							ruleBO = null;
							//enternalVariables = null;
							
						}//End of decision if
						
					}//End of insured loop
					
				}//End of insured if.
			}//End of for loop.
			
		}//End of Main if
		
			
			*//**
			 * Below code is used for the Client Match 
			 *//*
			if(clientMatch){
				for (int itr = 0; itr < responseObject.getClientData().getListInsured().size(); itr++) {
					Insured insured = (Insured) responseObject.getClientData().getListInsured().get(itr);					
					if (null != insured && null != insured.getListOverallDec()) {
						for (int insItr = 0; insItr < insured.getListOverallDec().size(); insItr++) {
							Decision decision = (Decision) insured.getListOverallDec().get(insItr);
							if (null != decision && null != decision.getProductName() && !"".equalsIgnoreCase(decision.getProductName().trim())) {
								productdecisionBuffer.append(decision.getDecision());
							}
						}
					}
				}
				
				
				if(null!=applicationDTO && null!=applicationDTO.getApplicant() && applicationDTO.getApplicant().size()>0){
					overallDecision = getoverallDecision(productdecisionBuffer);
					System.out.println("Aura decision for client match>>"+overallDecision);  
					
					for(int itr=0;itr<applicationDTO.getApplicant().size();itr++){
						applicantDTO = (ApplicantDTO)applicationDTO.getApplicant().get(itr);
						if(null!=applicantDTO 
								&& "Primary".equalsIgnoreCase(applicantDTO.getApplicantType())
								&& null!=applicantDTO.getFirstName()
								&& !"DCL".equalsIgnoreCase(overallDecision)
								//&& null!=applicantDTO.getLastName()
								&& null!=applicantDTO.getDateOfBirth()
								&& !("TCOVER".equalsIgnoreCase(applicationDTO.getRequestType()) || "ICOVER".equalsIgnoreCase(applicationDTO.getRequestType()) || "UWCOVER".equalsIgnoreCase(applicationDTO.getRequestType()))
								&& applicationDTO.getPolicyStartDate()==null
							){						
								inputData = new InputData();
								System.out.println("Inside clientsah dsafd sa");
								inputData.setFirstName(applicantDTO.getFirstName());
								//New WR to add last name logic into client match
								inputData.setSurName(applicantDTO.getLastName());
								if("COFS".equalsIgnoreCase(partner)){
									inputData.setSurName(applicantDTO.getLastName());
									inputData.setBrand("COFS");
								}
								inputData.setDob(applicantDTO.getDateOfBirth());
								inputData.setResponseObject(responseObject);
								inputData.setCurrentProductDecision(overallDecision);
								eapplyClientAgent = (EapplyClientAgent)ClientAgentHelper.getClientAgent(EapplyClientAgent.CLASS_NAME);
								outputData = eapplyClientAgent.searchAuraSession(username, authToken, null, lobclientMatch, "Retrieve", inputData);								
								if(null!=outputData){
									responseObject.setClientMatched(outputData.getClientMatched());
									if(responseObject.isClientMatched()){
										responseObject.setClientMatchReason("eApply client match");	
									}
									
								}
								
								*//**
								 * ODS search
								 *//*
								
								String ca = AuraRequestMapper.oDSSearch(applicantDTO.getFirstName(), applicantDTO.getLastName(), applicantDTO.getDateOfBirth());
								
								if(null != ca && (ca.contains("GL") || ca.contains("SCI"))){
									System.out.println("Going inside>>.");
									responseObject.setClientMatched(true);
									if(null != responseObject.getClientMatchReason()){
										ca = ca.substring(ca.indexOf("<message>")+9 , ca.lastIndexOf("</message>"));
										responseObject.setClientMatchReason(responseObject.getClientMatchReason()+" AND "+ca);
									}else{
										ca = ca.substring(ca.indexOf("<message>")+9 , ca.lastIndexOf("</message>"));
										responseObject.setClientMatchReason(ca);
									}
								}
								
								System.out.println("After Web test>>"+ca);
							
						}
					}
					
				}
			
			}
			
		LogHelper.methodExit(PACKAGE_NAME, CLASS_NAME, METHOD_NAME);
		
		
		auraService = null;
		productdecisionBuffer = null;
		return responseObject;
		
	}	*/
	
	
	/**
	 * This method makes a Aura webservice call to submit responses to Aura questionnaire.
	 * @param clientname
	 * @param questionnaireId
	 * @return
	 * @throws com.chordiant.service.ServiceException 
	 * @throws MetlifeException 
	 * @throws ServiceException 
	 * @throws ParserConfigurationException 
	 * @throws IOException 
	 * @throws SAXException 
	 */
	/*public ResponseObject processPostProcessing(ResponseObject responseObject, HashMap ruleMap, String lob, String partner, String username, String authToken, ApplicationDTO applicationDTO) throws com.chordiant.service.ServiceException, MetlifeException, ServiceException, SAXException, IOException, ParserConfigurationException {
		final String METHOD_NAME = "submitQuestionnaire";
		LogHelper.methodEntry(PACKAGE_NAME, CLASS_NAME, METHOD_NAME);
		if(null==auraService){
			auraService = getAuraService();
		}
		StringBuffer productdecisionBuffer = new StringBuffer();
		String overallDecision = null;		
		EapplyClientAgent eapplyClientAgent = null;
		ApplicantDTO applicantDTO = null;
		InputData inputData = null;
		OutputData outputData = null;
		PostProcessRuleBO  ruleBO = null;
		Boolean clientMatch = false;		
		List<String> ratingReason = null;
		List<String> plannedReason = null;
		String productOrgDecision = null;
		Rating rating = null;
		String lobclientMatch = null;
		List<ItemData> enternalVariables = null;
		double extIPcoverAmt = 0.0;
		CoversDTO coverDto = null;
		
		System.out.println("what is the client >>>"+responseObject.getClientData());
		System.out.println("what is the getQuestionnaireId >>>"+responseObject.getQuestionnaireId());
		
			if(null != responseObject.getClientData() && null != responseObject.getClientData().getQuestionFilter() && responseObject.getClientData().getQuestionFilter().contains("Group")){
				lob = "Group";
				lobclientMatch = "Institutional";
			}else{
				lob = "Direct";
				lobclientMatch = "Individual";
			}
			
			if(responseObject.getClientData().getAppSubSetCode().equalsIgnoreCase("3")
					|| responseObject.getClientData().getAppSubSetCode().equalsIgnoreCase("4") 
					|| responseObject.getClientData().getAppSubSetCode().equalsIgnoreCase("5")
					||"CITI".equalsIgnoreCase(partner) 
					||"ACRF".equalsIgnoreCase(partner) 
					|| "Group".equalsIgnoreCase(lob)){			
					
				String[] reasons;
				
			for (int itr = 0; itr < responseObject.getClientData().getListInsured().size(); itr++) {
				Insured insured = (Insured) responseObject.getClientData().getListInsured().get(itr);				
				
				if (null != insured && null != insured.getListOverallDec()) {
					for (int insItr = 0; insItr < insured.getListOverallDec().size(); insItr++) {
						Decision decision = (Decision) insured.getListOverallDec().get(insItr);
						if (null != decision && null != decision.getProductName() && !"".equalsIgnoreCase(decision.getProductName().trim())) {
							
							ratingReason = new ArrayList<String>();
							plannedReason = new ArrayList<String>();
							ruleBO = new PostProcessRuleBO();
							productOrgDecision= decision.getDecision();
							EapplicationIndividualRuleHelper.RULE_FILE_LOC = ConfigurationHelper.getConfigurationValue("RULE_FILE_LOC", "RULE_FILE_LOC");
							if(null!=ruleMap && null!=ruleMap.get("PostProcessingMaster.xls")){
								ruleBO.setKbase((KnowledgeBase)ruleMap.get("PostProcessingMaster.xls"));
							}
							ruleBO.setRuleFileName("PostProcessingMaster.xls");
							//Basic Data set Here like LOB, Partner, Product Name
							ruleBO.setBrand(partner);
							ruleBO.setLob(lob);
							ruleBO.setProduct(decision.getProductName());
							//Step1 fire the rule mater here .
							ruleBO.setRuleFileName("PostProcessingMaster.xls");
							ruleBO = EapplicationIndividualRuleHelper.invokepostProcessRule(ruleBO);
							clientMatch = ruleBO.getClientMatch();
							System.out.println("After the rule sheet<>>>"+clientMatch);
							if(null!=applicationDTO && null!=applicationDTO.getApplicant()){
								for (int applicantItr = 0; applicantItr < applicationDTO.getApplicant().size(); applicantItr++) {
									applicantDTO = (ApplicantDTO)applicationDTO.getApplicant().get(applicantItr);
									if(null!=applicantDTO && "Primary".equalsIgnoreCase(applicantDTO.getApplicantType()) 
											&& null!=applicantDTO.getTransferCvr() && applicantDTO.getTransferCvr()
											&& clientMatch){
										System.out.println("loop 111");
										clientMatch= Boolean.FALSE;
									}
									
									*//**
									 * If no existing cover then the application will be RUW in case of PWCS
									 *//*
									if(null != partner && "PWCS".equalsIgnoreCase(partner)){
										if (null != applicantDTO.getCovers()) {
											for(int itrcvr=0;itrcvr<applicantDTO.getCovers().size();itrcvr++){
												coverDto = (CoversDTO)applicantDTO.getCovers().get(itr);
												if("IP".equalsIgnoreCase(coverDto.getCoverNo()) && null != coverDto.getExistingCoverAmount() && !"".equalsIgnoreCase(coverDto.getExistingCoverAmount().trim())){
													extIPcoverAmt = new BigDecimal(coverDto.getExistingCoverAmount()).doubleValue();
												}
										}
									}
									}
								}
							}
							if(null!=applicationDTO.getRequestType() && ("UWCOVER".equalsIgnoreCase(applicationDTO.getRequestType())|| "ICOVER".equalsIgnoreCase(applicationDTO.getRequestType()))){
								System.out.println("Loop 2");
								clientMatch= Boolean.FALSE;
							}
							//Setting the data for the 2nd Rule
							ruleBO.setAuraPostProcessID(ruleBO.getAuraPostProcessID());
							ruleBO.setOriginalDecision(decision.getDecision());
							ruleBO.setTotalExlusion(decision.getListExclusion().size());
							ruleBO.setTotalLExlusion(decision.getLExclusions());
							ruleBO.setTotalMExlusion(decision.getMExclusions());
							
							System.out.println("ruleBO.setAuraPostProcessID"+ruleBO.getAuraPostProcessID());
							System.out.println("ruleBO.setAuraPostProcessID>."+ruleBO.getOriginalDecision());
							
							//Setting the loadings
							int loadingValue = 0;
							if(null != decision.getTotalDebitsValue()){
								loadingValue = new Double(decision.getTotalDebitsValue()).intValue();
								decision.setOriginalTotalDebitsValue(""+loadingValue);
							}
							ruleBO.setTotalLoading(loadingValue);
							//Setp2 fire rule 2 here
							EapplicationIndividualRuleHelper.RULE_FILE_LOC = ConfigurationHelper.getConfigurationValue("RULE_FILE_LOC", "RULE_FILE_LOC");
							if(null!=ruleMap && null!=ruleMap.get("PostProcessing1.xls")){
								ruleBO.setKbase((KnowledgeBase)ruleMap.get("PostProcessing1.xls"));
							}							
							ruleBO.setRuleFileName("PostProcessing1.xls");
							ruleBO = EapplicationIndividualRuleHelper.invokepostProcessRule(ruleBO);
							
							if(null != ruleBO.getPostAURADecision()){
								System.out.println("ruleBO.getPostAURADecision() 111 "+ruleBO.getPostAURADecision());
								decision.setDecision(ruleBO.getPostAURADecision());	
								reasons = new String[1];
								reasons[0] = ruleBO.getReason();
								decision.setReasons(reasons);	
								plannedReason.add(reasons[0]);
							}else{
								System.out.println("ruleBO.getPostAURADecision() 111 else"+ruleBO.getPostAURADecision());
							}
							
							
							
							ruleBO.setPostAURADecision(null);
							EapplicationIndividualRuleHelper.RULE_FILE_LOC = ConfigurationHelper.getConfigurationValue("RULE_FILE_LOC", "RULE_FILE_LOC");
							if(null!=ruleMap && null!=ruleMap.get("PostProcessing2.xls")){
								ruleBO.setKbase((KnowledgeBase)ruleMap.get("PostProcessing2.xls"));
							}							
							ruleBO.setRuleFileName("PostProcessing2.xls");
							ruleBO.setOriginalDecision(decision.getDecision());
							
							System.out.println("decision.getDecision() before 22"+decision.getDecision());
							
							//Fire rule 2s Here
							ruleBO = EapplicationIndividualRuleHelper.invokepostProcessRule(ruleBO);
							if(null != ruleBO.getPostAURADecision()){
								System.out.println("ruleBO.getPostAURADecision() 222>>"+ruleBO.getPostAURADecision());
								decision.setDecision(ruleBO.getPostAURADecision());	
								decision.setTotalDebitsReason(ruleBO.getReason());
								decision.setTotalDebitsValue(ruleBO.getPostAURALoading().toString());
								reasons = new String[1];
								reasons[0] = ruleBO.getReason();
								decision.setReasons(reasons);
								if(Double.parseDouble(decision.getTotalDebitsValue())>0.0){
									plannedReason.add(ruleBO.getReason());
								}
								
							}else{
								System.out.println("ruleBO.getPostAURADecision() 222 else>>"+ruleBO.getPostAURADecision());
							}
							productdecisionBuffer.append(decision.getDecision());
														
							*//**
							 * BMI post processing only for institutional now - 05012012 
							 *//*
							if(("group".equalsIgnoreCase(lob) && null != decision.getDecision()) || responseObject.getClientData().getAppSubSetCode().equalsIgnoreCase("5")){
								ruleBO.setPostAURADecision(null);
								EapplicationIndividualRuleHelper.RULE_FILE_LOC = ConfigurationHelper.getConfigurationValue("RULE_FILE_LOC", "RULE_FILE_LOC");
								if(null!=ruleMap && null!=ruleMap.get("PostProcessing3.xls")){
									ruleBO.setKbase((KnowledgeBase)ruleMap.get("PostProcessing3.xls"));
								}								
								ruleBO.setRuleFileName("PostProcessing3.xls");
								ruleBO.setOriginalDecision(decision.getDecision());
								ruleBO.setBrand(partner);
								*//** 
								 * Loop through the genderAgeBMI tag to get the BMI loadng  
								 *//*
							List<Decision> bmiList = null;
							if(responseObject.getClientData().getAppSubSetCode().equalsIgnoreCase("5")){
								bmiList = insured.getListGenderAgeBMIRangeTable();
							}else if ("group".equalsIgnoreCase(lob)){
								bmiList = insured.getListGenderAgeBMIDec();
							}
							
							int bmiLoadingValue = 0;
							int bmlMedicalLoading  = 0;
								for (int bmiins = 0; bmiins < bmiList.size(); bmiins++) {
									Decision bmidecision = bmiList.get(bmiins);
									
									if (null != bmidecision && null != bmidecision.getProductName() && null != bmidecision.getTotalDebitsValue() && !"".equalsIgnoreCase(bmidecision.getProductName().trim()) && decision.getProductName().equalsIgnoreCase(bmidecision.getProductName())) {
										if(null != bmidecision.getTotalDebitsValue()){
											bmiLoadingValue = new Double(bmidecision.getTotalDebitsValue()).intValue();
										}else{
											bmiLoadingValue = 0;
										}
										ruleBO.setBmiLoading(bmiLoadingValue);
										
										if(new BigDecimal(decision.getTotalDebitsValue()).intValue() >= bmiLoadingValue){
											bmlMedicalLoading = new BigDecimal(decision.getTotalDebitsValue()).subtract(new BigDecimal(bmidecision.getTotalDebitsValue())).intValue();	
										}
										
										//For BMI it should be Medical Loadings only.
										ruleBO.setTotalExlusion(decision.getMExclusions());
										
										
									}
								} // Endo of BMI LOOP
								int origLoadingValue = 0;
								for (int origins = 0; origins < insured.getListOriginalRuleDec().size(); origins++) {
									Decision origDecision = (Decision) insured.getListOriginalRuleDec().get(origins);
									
									if (null != origDecision && null != origDecision.getProductName() && !"".equalsIgnoreCase(origDecision.getProductName().trim()) && decision.getProductName().equalsIgnoreCase(origDecision.getProductName())) {
										
										if(null != origDecision.getTotalDebitsValue()){
											origLoadingValue = new Double(origDecision.getTotalDebitsValue()).intValue();
										}else{
											origLoadingValue = 0;
										}
									}
								}
								if(bmlMedicalLoading >= 0){
									ruleBO.setTotalLoading(bmlMedicalLoading);
									
								}else if(origLoadingValue > 0){
									ruleBO.setTotalLoading(origLoadingValue);
								}
								ruleBO = EapplicationIndividualRuleHelper.invokepostProcessRule(ruleBO);
								
								//capturing original decision from Aura
								if("ACC".equalsIgnoreCase(productOrgDecision) ){
									
									if(null!=decision.getListRating() && decision.getListRating().size()>0 ){
										for (int iter=0; iter<decision.getListRating().size(); iter++) {
											 rating = (Rating) decision.getListRating().get(iter);
											if(null!=rating && null!=rating.getReason() && rating.getReason().length()>0 && null!=decision.getTotalDebitsValue()
													&& decision.getTotalDebitsValue().length()>0 && Double.parseDouble(decision.getTotalDebitsValue()) > 0.0){
												ratingReason.add(rating.getReason());
											}									
										}
									}
									
									if(null!=decision.getListExclusion() && decision.getListExclusion().size()>0){
										for (Iterator iter = decision.getListExclusion()
												.iterator(); iter.hasNext();) {
											Exclusion exclusion = (Exclusion) iter.next();
											if(null!=exclusion && null!=exclusion.getReason()&& exclusion.getReason().length>0){
												for (int exItr = 0; exItr < exclusion.getReason().length; exItr++) {
													if(null!=exclusion.getReason()[exItr] && exclusion.getReason()[exItr].length()>0 && !"null".equalsIgnoreCase(exclusion.getReason()[exItr].toString())){
														ratingReason.add(exclusion.getReason()[exItr]);
													}
													
												}
											}
											
										}
										
									}
									decision.setOrgReason(ratingReason);
								}else if("DCL".equalsIgnoreCase(productOrgDecision)&& null!=decision.getReasons() && decision.getReasons().length>0){
									for (int exItr = 0; exItr < decision.getReasons().length; exItr++) {
										ratingReason.add(decision.getReasons()[exItr]);
									}
									decision.setOrgReason(ratingReason);
								}else if("RUW".equalsIgnoreCase(productOrgDecision)){
									
									for (int iter=0; iter<decision.getListRating().size(); iter++) {
										 rating = (Rating) decision.getListRating().get(iter);
										if(null!=rating && null!=rating.getReason() && rating.getReason().length()>0 && null!=decision.getTotalDebitsValue()
												&& decision.getTotalDebitsValue().length()>0 && Double.parseDouble(decision.getTotalDebitsValue()) > 0.0){
											ratingReason.add(rating.getReason());
										}									
									}
									decision.setOrgReason(ratingReason);
								}
								
								if(null != ruleBO.getPostAURADecision()){
									decision.setDecision(ruleBO.getPostAURADecision());	
									reasons = new String[1];
									reasons[0] = ruleBO.getReason();
									decision.setReasons(reasons);
									plannedReason.add(ruleBO.getReason());
								}
								
								if(null!=plannedReason && plannedReason.size()>0){
									decision.setPlannedReason(plannedReason);
								}
								
								 // END of GROUP BMI Loop	
							}else{
								System.out.println("No third post process");
							}
						
							*//**
							 * Level 4 postProcessing for 15hrs questions.
							 *//*
							if("group".equalsIgnoreCase(lob) && null != partner && "SFPS".equalsIgnoreCase(partner) && null != decision.getProductName() && decision.getProductName().contains("IP") && "ACC".equalsIgnoreCase(decision.getDecision())){
								enternalVariables = insured.getListExtEngineVar();
								ItemData im = null;
								for(int l=0;l<enternalVariables.size();l++){									
									 im = enternalVariables.get(l);
									if(null != im.getCode() && "Hours15".equalsIgnoreCase(im.getCode()) && null != im.getValue() && "No".equalsIgnoreCase(im.getValue())){
											decision.setDecision("RUW");	
											reasons = new String[1];
											reasons[0] = "RUW, 15 hour eligibility";
											decision.setReasons(reasons);
											plannedReason.add(ruleBO.getReason());
										
										if(null!=plannedReason && plannedReason.size()>0){
											decision.setPlannedReason(plannedReason);
										}
									}
									
								}
							}
							
							*//**
							 * Level 5 postProcessing - only for PWCP Staff, When there is a existing cover will RUW the case
							 *//*
							if("group".equalsIgnoreCase(lob) && null != partner && "PWCS".equalsIgnoreCase(partner) && extIPcoverAmt <= 0 && "ACC".equalsIgnoreCase(decision.getDecision())){
								decision.setDecision("RUW");	
								reasons = new String[1];
								reasons[0] = "RUW, existing cover nill";
								decision.setReasons(reasons);
								plannedReason.add(ruleBO.getReason());
							
								if(null!=plannedReason && plannedReason.size()>0){
									decision.setPlannedReason(plannedReason);
								
								}
							}
							
							*//**
							 * Level 6 postProcessing - When there is a change to conevrt to fixed from unitised
							 *//*
							if("group".equalsIgnoreCase(lob)){							
								
								if(null!=applicationDTO && null!=applicationDTO.getApplicant()){
									for (int applicantItr = 0; applicantItr < applicationDTO.getApplicant().size(); applicantItr++) {
										if (null != applicantDTO.getCovers()) {
											for(int itrcvr=0;itrcvr<applicantDTO.getCovers().size();itrcvr++){
												coverDto = (CoversDTO)applicantDTO.getCovers().get(itrcvr);	
												if(null!=coverDto && applicantDTO.getAuradetails()!=null &&  "long".equalsIgnoreCase(applicantDTO.getAuradetails().getFormLength()) && coverDto.getCoverCategory()!=null && !coverDto.getCoverCategory().contains("Cancel")){
													// setting existing unit indicator		
													if(null != coverDto.getCoverExitUnit() && !"".equalsIgnoreCase(coverDto.getCoverExitUnit().trim()) && new BigDecimal(""+coverDto.getCoverExitUnit()).doubleValue()>0){
														ruleBO.setExistingUnit(Boolean.TRUE);
													}else{
														ruleBO.setExistingUnit(Boolean.FALSE);
													}												
													//setting additional unit indicator													
													ruleBO.setAdditionalUnit(coverDto.getAdditionalUnitInd());
													ruleBO.setPostAURADecision(null);
													EapplicationIndividualRuleHelper.RULE_FILE_LOC = ConfigurationHelper.getConfigurationValue("RULE_FILE_LOC", "RULE_FILE_LOC");											
													if(null!=ruleMap && null!=ruleMap.get("PostProcessing4.xls")){
														ruleBO.setKbase((KnowledgeBase)ruleMap.get("PostProcessing4.xls"));
													}								
													ruleBO.setRuleFileName("PostProcessing4.xls");
													ruleBO.setLob(lob);
													ruleBO.setBrand(partner);
													ruleBO.setRequestType("NA");
													ruleBO = EapplicationIndividualRuleHelper.invokepostProcessRule(ruleBO);
													if(null != ruleBO.getPostAURADecision() && "RUW".equalsIgnoreCase(ruleBO.getPostAURADecision())){
														decision.setDecision(ruleBO.getPostAURADecision());							
														reasons = new String[1];
														reasons[0] = ruleBO.getReason();
														decision.setReasons(reasons);							
														plannedReason.add(ruleBO.getReason());													
														if(null!=plannedReason && plannedReason.size()>0){
															decision.setPlannedReason(plannedReason);														
														}
														break;
													}
												}
											}
										}
									}
								}							
								
							}
							*//**
							 * Level 7 postProcessing - For transfers and Life events
							 *//*
							if("group".equalsIgnoreCase(lob)){	
								if(null!=applicationDTO && null!=applicationDTO.getRequestType()){
									EapplicationIndividualRuleHelper.RULE_FILE_LOC = ConfigurationHelper.getConfigurationValue("RULE_FILE_LOC", "RULE_FILE_LOC");
									if(null!=ruleMap && null!=ruleMap.get("PostProcessing4.xls")){
										ruleBO.setKbase((KnowledgeBase)ruleMap.get("PostProcessing4.xls"));
									}								
									ruleBO.setRuleFileName("PostProcessing4.xls");
									ruleBO.setExistingUnit(false);
									ruleBO.setAdditionalUnit(false);
									ruleBO.setLob(lob);
									ruleBO.setBrand(partner);
									ruleBO.setRequestType(applicationDTO.getRequestType());
									System.out.println("applicationDTO.getRequestType()>>"+applicationDTO.getRequestType());
									ruleBO = EapplicationIndividualRuleHelper.invokepostProcessRule(ruleBO);
									if(null != ruleBO.getPostAURADecision() && "RUW".equalsIgnoreCase(ruleBO.getPostAURADecision())){
										decision.setDecision(ruleBO.getPostAURADecision());							
										System.out.println("ruleBO.getPostAURADecision()>>"+ruleBO.getPostAURADecision());
										reasons = new String[1];
										reasons[0] = ruleBO.getReason();
										decision.setReasons(reasons);							
										plannedReason.add(ruleBO.getReason());													
										if(null!=plannedReason && plannedReason.size()>0){
											decision.setPlannedReason(plannedReason);														
										}
									}
								}
							}							
							
							ruleBO = null;
							
						}//End of decision if
						
					}//End of insured loop
					
				}//End of insured if.
			}//End of for loop.
			
		}//End of Main if
		
			
			*//**
			 * Below code is used for the Client Match 
			 *//*
			if(clientMatch){
				for (int itr = 0; itr < responseObject.getClientData().getListInsured().size(); itr++) {
					Insured insured = (Insured) responseObject.getClientData().getListInsured().get(itr);					
					if (null != insured && null != insured.getListOverallDec()) {
						for (int insItr = 0; insItr < insured.getListOverallDec().size(); insItr++) {
							Decision decision = (Decision) insured.getListOverallDec().get(insItr);
							if (null != decision && null != decision.getProductName() && !"".equalsIgnoreCase(decision.getProductName().trim())) {
								productdecisionBuffer.append(decision.getDecision());
							}
						}
					}
				}
				
				
				if(null!=applicationDTO && null!=applicationDTO.getApplicant() && applicationDTO.getApplicant().size()>0){
					overallDecision = getoverallDecision(productdecisionBuffer);
					for(int itr=0;itr<applicationDTO.getApplicant().size();itr++){
						applicantDTO = (ApplicantDTO)applicationDTO.getApplicant().get(itr);
						if(null!=applicantDTO 
								&& "Primary".equalsIgnoreCase(applicantDTO.getApplicantType())
								&& null!=applicantDTO.getFirstName()
							//	&& null!=applicantDTO.getLastName()
								&& null!=applicantDTO.getDateOfBirth()
								&& applicationDTO.getPolicyStartDate()==null
								&& !("TCOVER".equalsIgnoreCase(applicationDTO.getRequestType()) || "ICOVER".equalsIgnoreCase(applicationDTO.getRequestType()) || "UWCOVER".equalsIgnoreCase(applicationDTO.getRequestType()))
										){						
								inputData = new InputData();
								inputData.setFirstName(applicantDTO.getFirstName());
								//New WR to add last name logic into client match
								inputData.setSurName(applicantDTO.getLastName());
								if("COFS".equalsIgnoreCase(partner)){
									inputData.setSurName(applicantDTO.getLastName());
									inputData.setBrand("COFS");
								}
								
								inputData.setDob(applicantDTO.getDateOfBirth());
								inputData.setResponseObject(responseObject);
								inputData.setCurrentProductDecision(overallDecision);
								eapplyClientAgent = (EapplyClientAgent)ClientAgentHelper.getClientAgent(EapplyClientAgent.CLASS_NAME);
								outputData = eapplyClientAgent.searchAuraSession(username, authToken, null, lobclientMatch, "Retrieve", inputData);								
								if(null!=outputData){
									responseObject.setClientMatched(outputData.getClientMatched());
									if(responseObject.isClientMatched()){
										responseObject.setClientMatchReason("eApply client match");	
									}
								}
								
								
								*//**
								 * ODS search
								 *//*
								
								String ca = AuraRequestMapper.oDSSearch(applicantDTO.getFirstName(), applicantDTO.getLastName(), applicantDTO.getDateOfBirth());
								
								if(null != ca && (ca.contains("GL") || ca.contains("SCI"))){
									responseObject.setClientMatched(true);
									
									if(null != responseObject.getClientMatchReason()){
										ca = ca.substring(ca.indexOf("<message>")+9 , ca.lastIndexOf("</message>"));
										responseObject.setClientMatchReason(responseObject.getClientMatchReason()+" AND "+ca);
									}else{
										ca = ca.substring(ca.indexOf("<message>")+9 , ca.lastIndexOf("</message>"));
										responseObject.setClientMatchReason(ca);
									}
									
								}
								
								System.out.println("After Web test>>"+ca);
							
						}
					}
					
				}
			
			}
			
		LogHelper.methodExit(PACKAGE_NAME, CLASS_NAME, METHOD_NAME);
		
		
		auraService = null;
		productdecisionBuffer = null;
		return responseObject;}	*/
	
	/*private static String getoverallDecision(StringBuffer strBuff){
		final String METHOD_NAME = "getoverallDecision";		
		LogHelper.methodEntry(PACKAGE_NAME, CLASS_NAME, METHOD_NAME);	
		String decision =null;
		  if(null!=strBuff && !"".equalsIgnoreCase(strBuff.toString())){   
			  if(strBuff.toString().contains("RUW")){
				  decision = "RUW";				
              }else if(!strBuff.toString().contains("ACC") && strBuff.toString().contains("DCL")){
            	  decision = "DCL";            	 
              }else if(strBuff.toString().contains("ACC") && strBuff.toString().contains("DCL")){
            	  decision="ACC";            	 
              }else{
            	  decision="ACC";            	  
              }
		  }
		  LogHelper.methodExit(PACKAGE_NAME, CLASS_NAME, METHOD_NAME);
		  return decision;
	}*/

	/**
	 * This method makes a Aura webservice call to save Aura questionnaire.
	 * @param clientname
	 * @param questionnaireId
	 * @return
	 * @throws ParserConfigurationException 
	 * @throws IOException 
	 * @throws SAXException 
	 * @throws ServiceException 
	 */
	
	public ResponseObject saveQuestionnaire(String clientname, String questionnaireId) throws SAXException, IOException, ParserConfigurationException, ServiceException {
		final String METHOD_NAME = "saveQuestionnaire";
		if(null==auraService){
			auraService = getAuraService();
		}
		String output = auraService.saveAndExitQuestionnaire(clientname, questionnaireId);
		ResponseObject responseObject = AuraResponseMapper.populateResponse(output);
		responseObject.setQuestionnaireId(questionnaireId);
		auraService = null;
		return responseObject;
	}


	public AuraService getAuraService() throws MalformedURLException, ServiceException {
		String[] auraURL = {"-lhttps://www.e2e.underwriting.metlife.com.au/MetAus/services/AuraService"};
		Options options = null;
		AuraServiceProxyLocator auraSevProxyLocater = null;
		try {
			options = new Options(auraURL);
			String serviceURL = options.getURL();
			URL url = new URL(serviceURL);
			auraService = null;
			auraSevProxyLocater = null;
			auraSevProxyLocater = new AuraServiceProxyLocator();
			auraSevProxyLocater.setMaintainSession(true);
			auraService = auraSevProxyLocater.getAuraService(url);
		} catch (MalformedURLException e) {
			throw e;
		} catch (ServiceException e) {
			throw e;
		}finally{
			auraSevProxyLocater = null;
			options = null;
		}
		return auraService;
	}
}
