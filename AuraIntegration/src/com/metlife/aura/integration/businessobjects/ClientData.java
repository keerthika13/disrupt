package com.metlife.aura.integration.businessobjects;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
public class ClientData implements Serializable{
	private String questionFilter;
	private String sessionId;
	private String appExtractLan;
	private String appIntMode;
	private String appSetCode;
	private String appSubSetCode;
	private String appVersion;
	
	private List<Insured> listInsured;
	private List<ItemData> listIntEngineVar;
	private List<ItemData> listExtEngineVar;

	public List<ItemData> getListExtEngineVar() {
		
		if (null == listExtEngineVar) {
			listExtEngineVar = new ArrayList<ItemData>();
		}
		return listExtEngineVar;
	}

	public List<ItemData> getListIntEngineVar() {
		
		if (null == listIntEngineVar) {
			listIntEngineVar = new ArrayList<ItemData>();
		}
		return listIntEngineVar;
	}

	public String getAppExtractLan() {
		return appExtractLan;
	}

	public void setAppExtractLan(String appExtractLan) {
		this.appExtractLan = appExtractLan;
	}

	public String getAppIntMode() {
		return appIntMode;
	}

	public void setAppIntMode(String appIntMode) {
		this.appIntMode = appIntMode;
	}

	public String getAppSetCode() {
		return appSetCode;
	}

	public void setAppSetCode(String appSetCode) {
		this.appSetCode = appSetCode;
	}

	public String getAppSubSetCode() {
		return appSubSetCode;
	}

	public void setAppSubSetCode(String appSubSetCode) {
		this.appSubSetCode = appSubSetCode;
	}

	public String getAppVersion() {
		return appVersion;
	}

	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}

	public String getQuestionFilter() {
		return questionFilter;
	}

	public void setQuestionFilter(String questionFilter) {
		this.questionFilter = questionFilter;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public List<Insured> getListInsured() {
		
		if (null == listInsured) {
			listInsured = new ArrayList<Insured>();
		}
		return listInsured;
	} 
}
