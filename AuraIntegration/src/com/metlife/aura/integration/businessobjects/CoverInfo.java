package com.metlife.aura.integration.businessobjects;

public class CoverInfo {
	
	private String coverName = null;
	private String coverAmount = null;
	private String coverNo = null;
	private String benefitPeriod = null;
	private String waitingPeriod = null;	
	private double existingAmount ;
	private Boolean riskIncrDueToWP_Decr_or_BP_Incr = null;
	
	
	public Boolean getRiskIncrDueToWP_Decr_or_BP_Incr() {
		return riskIncrDueToWP_Decr_or_BP_Incr;
	}
	public void setRiskIncrDueToWP_Decr_or_BP_Incr(
			Boolean riskIncrDueToWP_Decr_or_BP_Incr) {
		this.riskIncrDueToWP_Decr_or_BP_Incr = riskIncrDueToWP_Decr_or_BP_Incr;
	}
	
	public String getBenefitPeriod() {
		return benefitPeriod;
	}
	public void setBenefitPeriod(String benefitPeriod) {
		this.benefitPeriod = benefitPeriod;
	}
	public String getWaitingPeriod() {
		return waitingPeriod;
	}
	public void setWaitingPeriod(String waitingPeriod) {
		this.waitingPeriod = waitingPeriod;
	}
	public String getCoverAmount() {
		return coverAmount;
	}
	public void setCoverAmount(String coverAmount) {
		this.coverAmount = coverAmount;
	}
	public String getCoverName() {
		return coverName;
	}
	public void setCoverName(String coverName) {
		this.coverName = coverName;
	}
	public String getCoverNo() {
		return coverNo;
	}
	public void setCoverNo(String coverNo) {
		this.coverNo = coverNo;
	}
	public double getExistingAmount() {
		return existingAmount;
	}
	public void setExistingAmount(double existingAmount) {
		this.existingAmount = existingAmount;
	}

}
