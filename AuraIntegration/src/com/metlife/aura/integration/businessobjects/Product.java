package com.metlife.aura.integration.businessobjects;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Product implements Serializable{
	private String ageCovAmt;
	private String ageDistCovAmt;
	private String coverageAmt;
	private String finCovAmt;
	private String value;
	
	public String getAgeCovAmt() {
		return ageCovAmt;
	}
	public void setAgeCovAmt(String ageCovAmt) {
		this.ageCovAmt = ageCovAmt;
	}
	public String getAgeDistCovAmt() {
		return ageDistCovAmt;
	}
	public void setAgeDistCovAmt(String ageDistCovAmt) {
		this.ageDistCovAmt = ageDistCovAmt;
	}
	public String getCoverageAmt() {
		return coverageAmt;
	}
	public void setCoverageAmt(String coverageAmt) {
		this.coverageAmt = coverageAmt;
	}
	public String getFinCovAmt() {
		return finCovAmt;
	}
	public void setFinCovAmt(String finCovAmt) {
		this.finCovAmt = finCovAmt;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
}
