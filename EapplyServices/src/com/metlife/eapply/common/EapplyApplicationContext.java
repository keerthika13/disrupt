/**
 * 
 */
package com.metlife.eapply.common;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class EapplyApplicationContext {
	
	//private static ApplicationContext context = new ClassPathXmlApplicationContext("/applicationContext.xml");
	
	public static Object getBean(String name){
		ApplicationContext context = AppContext.getApplicationContext();  
		return context.getBean(name);

	}

}
