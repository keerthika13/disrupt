package com.metlife.eapply.common;

import java.io.InputStream;
import java.util.Properties;
/**
 * This class is used to read the property file and store the parameters
 * Currently in our system, we use one property file. The property object is a public variable which can be accessed
 * directly.
 * This class is a singleton class.
 */
public class SystemProperty
{
	String CLASS_NAME = "SystemProperty";

	/**Only object of the singleton class */
	private static SystemProperty obj_systemProperty;

	/**Property object for the system property file*/
	private java.util.Properties obj_Properties;

	/**Name of the properties file for the batch process*/
	private String PROPERTY_FILE = "/System.properties";

	// private String EMAIL_PROPERTY_FILE = "CAWEmail.properties";
//	private Logger logger = new Logger(CLASS_NAME);

	/**
	 * PropertyFileParameters constructor comment.
	 * This method reads the property file and creates a properties object for it
	 * @param - none
	 * @return - none
	 * @exception - java.lang.Exception
	 */
	private SystemProperty() throws java.lang.Exception
	{
		loadProperties();
	}

	/**
	 * PropertyFileParameters reads from properties file and initializes the project
	 * @param - none
	 * @return - none
	 * @exception - java.lang.Exception
	 */
	public void loadProperties() throws java.lang.Exception
	{
		//if the properties object is null then there is an error
		//thus throw an exception
		obj_Properties = getPropertySet();		
		if(obj_Properties  == null)
		{
			throw(new Exception("System not initialized properly. Terminating process."));
		}
	}

	/**
	 * returns the only instance of this class
	 * @param - none
	 * @return - PropertyFileParameters object
	 * @exception - java.lang.Exception
	 */
	public static SystemProperty getInstance() throws Exception
	{
		if(obj_systemProperty ==  null)
		{
			obj_systemProperty = new SystemProperty();
		}

		return obj_systemProperty;
	}

	/**
	 * Gets the specified property
	 * @return java.lang.String
	 * @param arg_property java.lang.String
	 */
	public String getProperty(String arg_property)
	{
		return obj_Properties.getProperty(arg_property);
	}

	/**
	 * Gets the list of keys for all the properties
	 * @return java.lang.String
	 * @param arg_property java.lang.String
	 */
	public java.util.Enumeration getPropertyKeyList()
	{
		return obj_Properties.propertyNames();
	}

	/**
	 * Reads from the property file and returns a properties object
	 * @param - file name
	 * @return - Properties object for that file
	 * @exception -  java.io.FileNotFoundException,java.io.IOException
	 */
	private Properties getPropertySet() throws java.io.FileNotFoundException, java.io.IOException
	{
		  //System.out.println("System property file load starts");
		  InputStream inputStream = getClass().getResourceAsStream(PROPERTY_FILE);
		//System.out.println("System property file load ends");
	   //   InputStream emailInputStream = ClassLoader.getSystemResourceAsStream(EMAIL_PROPERTY_FILE);

		// if the file does not exist then return null else load the props and return the object.
		Properties obj_props = null;

		
		if (inputStream != null)
		{
			System.out.println("input stream is not null - good ");
			obj_props = new Properties();

			obj_props.load(inputStream);

			inputStream.close();
		}
		else
		{
			//System.out.println("input stream is  null - this is bad 99999999999");
//			logger.log(Priority.ERROR, PROPERTY_FILE+": does not exist");
		}

	
		return obj_props;
	}

	/**
	 * gets Integer property
	 * @return java.lang.Integer
	 * @param arg_property java.lang.String
	 */
	public int getIntegerProperty(String arg_property)
	{
		return Integer.parseInt(obj_Properties.getProperty(arg_property));
	}
}
